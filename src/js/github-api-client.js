export default class GitHubAPIClient {
    constructor({id, secret}) {
        this.id = id;
        this.secret = secret;
        this.reposCount = 5;
        this.reposSort = 'created asc';
    }

    async getUserData(user) {
        const userDataResponse = await fetch(`https://api.github.com/users/${user}?client_id=${this.id}&client_secret=${this.secret}`);
        const reposDataResponse = await fetch(`https://api.github.com/users/${user}/repos?per_page=${this.reposCount}&sort=${this.reposSort}&client_id=${this.id}&client_secret=${this.secret}`);

        return {
            profile: await userDataResponse.json(),
            repos: await reposDataResponse.json()
        }
    }
};
