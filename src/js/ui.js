export default class UI {
    constructor() {
        this.profileElt = document.getElementById('profile');
        this.searchElt = document.getElementById('search');
        this.alertElt = document.createElement('div');
    }

    showAlert(message, type) {
        this.clearProfile();

        this.alertElt.remove();
        this.alertElt.className = `alert alert-${type} mt-3 mb-0`;
        this.alertElt.textContent = message;
        this.searchElt.appendChild(this.alertElt);

        setTimeout(() => {
            this.alertElt.remove();
        }, 3000);
    }

    clearProfile() {
        while (this.profileElt.firstChild) {
            this.profileElt.removeChild(this.profileElt.firstChild);
        }
    }

    showProfile(profile) {
        this.alertElt.remove();

        this.profileElt.innerHTML = `
            <div class="card card-body mb-3">
              <div class="row">
                <div class="col-md-3">
                  <img class="img-fluid mb-2" src="${profile.avatar_url}">
                  <a href="${profile.html_url}" target="_blank" class="btn btn-primary btn-block mb-4 mb-md-0">View profile</a>
                </div>

                <div class="col-md-9">
                  <span class="badge badge-primary">Public repositories: ${profile.public_repos}</span>
                  <span class="badge badge-secondary">Public gists: ${profile.public_gists}</span>
                  <span class="badge badge-success">Followers: ${profile.followers}</span>
                  <span class="badge badge-info">Following: ${profile.following}</span>

                  <ul class="list-group mt-4">
                    <li class="list-group-item">Company: ${profile.company}</li>
                    <li class="list-group-item">Website/Blog: ${profile.blog}</li>
                    <li class="list-group-item">Location: ${profile.location}</li>
                    <li class="list-group-item">Member since: ${profile.created_at}</li>
                  </ul>
                </div>
              </div>
            </div>

            <h2 class="h3 page-heading mb-3">Latest repositories</h2>
            <div id="repos"></div>
        `;
    }

    showRepos(repos) {
        let output = '';

        repos.forEach((repo) => {
            output += `
                <div class="card card-body mb-2">
                  <div class="row">
                    <div class="col-md-6">
                      <a href="${repo.html_url}" target="_blank">${repo.name}</a>
                    </div>

                    <div class="col-md-6">
                      <span class="badge badge-primary">Stars: ${repo.stargazers_count}</span>
                      <span class="badge badge-secondary">Watchers: ${repo.watchers_count}</span>
                      <span class="badge badge-success">Forks: ${repo.forks_count}</span>
                    </div>
                  </div>
                </div>
            `;
        });

        document.getElementById('repos').innerHTML = output;
    }
};
