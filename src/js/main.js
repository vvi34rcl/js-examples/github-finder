import GitHubAPIClient from './github-api-client.js';
import UI from './ui.js';


const gitHubAPIClient = new GitHubAPIClient({
    id: '95493f32047418984c21',
    secret: 'fc99d7951bc9c2a5adde697ebf67e6b64529e92f'
});
const ui = new UI();
const searchUserInput = document.getElementById('search-user');


searchUserInput.addEventListener('keyup', function(event) {
    const user = event.target.value;

    if (user === '') {
        ui.clearProfile();

        return;
    }

    gitHubAPIClient.getUserData(user)
        .then((userData) => {
            if (userData.profile.message === 'Not Found') {
                ui.showAlert('User not found.', 'danger');

                return;
            }

            ui.showProfile(userData.profile);
            ui.showRepos(userData.repos);
        });
});
